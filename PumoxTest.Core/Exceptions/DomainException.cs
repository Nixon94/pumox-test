﻿using System;

namespace PumoxTest.Core.Exceptions
{
    public class DomainException : ApplicationException
    {
        public DomainException(string message)
            : base(message) { }
    }
}
