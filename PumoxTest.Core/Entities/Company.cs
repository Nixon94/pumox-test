﻿using PumoxTest.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PumoxTest.Core.Entities
{
    public class Company
    {
        public Company(long id, string name, int establishmentYear, IEnumerable<Employee> employees)
        {
            Id = id;
            SetName(name);
            SetEstablishmentYear(establishmentYear);
            _employeeList = employees?.ToList() ?? new List<Employee>();
        }
        private readonly IList<Employee> _employeeList;
        public long Id { get; }
        public string Name { get; private set; }
        public int EstablishmentYear { get; private set; }
        public IEnumerable<Employee> Employees => _employeeList;

        public void SetName(string name)
        {
            if (name is null)
                throw new DomainException("Company name can't be empty");
            Name = name;
        }

        public void SetEstablishmentYear(int year)
        {
            // 1600 - 1st company in the world was established "East India Company"
            if (year > DateTime.Now.Year || year < 1600)
                throw new DomainException("Year of establishment is not correct");
            EstablishmentYear = year;
        }
  
        private void IsEmployeeCorrect(Employee employee)
        {
            if (String.IsNullOrEmpty(employee.FirstName) || String.IsNullOrEmpty(employee.LastName))
                throw new DomainException("Employee first and second name can't be blank");
        }

        public void AddSingleEmployee(Employee employee)
        {
            IsEmployeeCorrect(employee);
            if (!_employeeList.Contains(employee))
                _employeeList.Add(employee);
        }

        public void AddEmployees(IEnumerable<Employee> employees)
        {
            if (employees.Any())
                foreach (var employee in employees)
                    AddSingleEmployee(employee);
        }


    }
}
