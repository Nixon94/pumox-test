﻿using PumoxTest.Core.Enums;
using PumoxTest.Core.Exceptions;
using System;

namespace PumoxTest.Core.Entities
{
    public class Employee : IEquatable<Employee>
    {
        public Employee(string firstName,  string lastName, long companyId, JobTitle jobTitle, DateTime dateOfBirth)
        {
            SetFirstName(firstName);
            SetLastName(lastName);
            CompanyId = companyId;
            JobTitle = jobTitle;
            DateOfBirth = dateOfBirth;
        }

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public long CompanyId { get;  set; }
        public JobTitle JobTitle { get; private set; }
        public DateTime DateOfBirth { get; private set; }

        public bool Equals(Employee other)
        {
            return other == null
                ? false
                : this.FirstName == other.FirstName &&
                   this.LastName == other.LastName &&
                   this.JobTitle == other.JobTitle &&
                   this.DateOfBirth == other.DateOfBirth;
        }

        private void SetFirstName(string firstName)
        {
            if(firstName is null)
                throw new DomainException("Employee name can't be blank");
            FirstName = firstName;
        }
        private void SetLastName(string lastName)
        {
            if (lastName is null)
                throw new DomainException("Employee surname can't be blank");
            LastName = lastName;
        }
    }
}
