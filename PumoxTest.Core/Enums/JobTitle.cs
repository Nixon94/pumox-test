﻿namespace PumoxTest.Core.Enums
{
    public enum JobTitle
    {
        Developer,
        Architect,
        Manager,
        Administrator
    };
}
