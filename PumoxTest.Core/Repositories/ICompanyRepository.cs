﻿using PumoxTest.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PumoxTest.Core.Repositories
{
    public interface ICompanyRepository
    {
        Task<Company> GetAsync(long id);
        Task AddAsync(Company company);
        Task UpdateAsync(Company company);
        Task DeleteAsync(Company company);
    }
}
