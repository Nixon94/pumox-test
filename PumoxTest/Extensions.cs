﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using ZNetCS.AspNetCore.Authentication.Basic;
using ZNetCS.AspNetCore.Authentication.Basic.Events;

namespace PumoxTest.Api
{
    public static class Extensions
    {
        public static T Bind<T>(this T model, Expression<Func<T, object>> selector, object value)
            => model.Bind<T, object>(selector, value);

        public static T BindId<T>(this T model, Expression<Func<T, Guid>> selector)
            => model.Bind(selector, Guid.NewGuid());

        private static TModel Bind<TModel, TProperty>(this TModel model, Expression<Func<TModel, TProperty>> expression, object value)
        {
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression is null)
            {
                memberExpression = ((UnaryExpression)expression.Body).Operand as MemberExpression;
            }
            var propertyName = memberExpression.Member.Name.ToLowerInvariant();

            var modelType = model.GetType();
            var field = modelType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic)
                .SingleOrDefault(x => x.Name.ToLowerInvariant().StartsWith($"<{propertyName}>"));
            if (field is null)
            {
                return model;
            }
            field.SetValue(model, value);
            return model;
        }

        public static void AddAuthentication(this IServiceCollection services)
        {
            services
           .AddAuthentication(BasicAuthenticationDefaults.AuthenticationScheme)
           .AddBasicAuthentication(
           options =>
           {
               options.Realm = "PumoxTest";
               options.Events = new BasicAuthenticationEvents
               {
                   OnValidatePrincipal = context =>
                   {
                       if ((context.UserName == "Pumox") && (context.Password == "Password"))
                       {
                           var claims = new List<Claim>
                           {
                                new Claim(ClaimTypes.Name, context.UserName, context.Options.ClaimsIssuer)
                           };

                           var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, context.Scheme.Name));
                           context.Principal = principal;
                       }
                       else
                           context.AuthenticationFailMessage = "Failed to authenitcate the user.";

                       return Task.CompletedTask;
                   }
               };
           });
        }
    }
}
