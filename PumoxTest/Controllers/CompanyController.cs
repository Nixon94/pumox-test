﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PumoxTest.Application.Commands.Companies;
using PumoxTest.Application.Commands.Dispatchers;
using PumoxTest.Application.DTO;
using PumoxTest.Application.Queries.Companies;
using PumoxTest.Application.Queries.Dispatchers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PumoxTest.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;

        public CompanyController(ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<CompanyDto>> GetById([FromRoute] GetCompany query)
        {
            var result = await _queryDispatcher.DispatchAsync<GetCompany, CompanyDto>(query);

            if (result is null) return NotFound();

            return Ok(result);
        }

        [HttpPost]
        [Route("search")]
        public async Task<ActionResult<IEnumerable<CompanyDto>>> SearchAsync([FromBody] SearchCompanies query)
        {
            var result = await _queryDispatcher
                .DispatchAsync<SearchCompanies, IEnumerable<CompanyDto>>(query);

            if (result is null || !result.Any()) return NotFound();

            return Ok(result);
        }

        [Authorize]
        [Produces("application/json")]
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateCompany command)
        {
            await _commandDispatcher.DispatchAsync(command);
            return CreatedAtAction(nameof(GetById), new { id = command.Id }, new { Id = command.Id });
        }

        [Authorize]
        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute] long id)
        {
            await _commandDispatcher.DispatchAsync(new DeleteCompany(id));
            return Ok();
        }

        [Authorize]
        [HttpPut]
        [Route("update/{id}")]
        public async Task<IActionResult> UpdateAsync([FromRoute] long id, [FromBody] UpdateCompany command)
        {
            command.Bind(cmd => cmd.Id, id);
            await _commandDispatcher.DispatchAsync(command);
            return Ok();
        }

    }
}
