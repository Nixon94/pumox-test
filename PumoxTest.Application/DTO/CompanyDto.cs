﻿using PumoxTest.Core.Entities;
using System.Collections.Generic;

namespace PumoxTest.Application.DTO
{
    public class CompanyDto
    {
        public long Id { get; set; }
        public string Name { get;  set; }
        public int EstablishmentYear { get;  set; }
        public IEnumerable<EmployeeDto> Employees { get; set; }
    }
}
