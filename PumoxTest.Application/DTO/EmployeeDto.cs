﻿using PumoxTest.Core.Enums;
using System;

namespace PumoxTest.Application.DTO
{
    public class EmployeeDto
    { 
        public string FirstName { get;  set; }
        public string LastName { get;  set; }
        public JobTitle JobTitle { get;  set; }
        public DateTime DateOfBirth { get;  set; }
    }
}
