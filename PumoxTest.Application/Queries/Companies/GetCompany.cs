﻿using PumoxTest.Application.DTO;

namespace PumoxTest.Application.Queries.Companies
{

    public class GetCompany : IQuery<CompanyDto>
    {
        public long Id { get; set; }
    }
}
