﻿using PumoxTest.Application.DTO;
using PumoxTest.Core.Enums;
using System;
using System.Collections.Generic;

namespace PumoxTest.Application.Queries.Companies
{
    public class SearchCompanies : IQuery<IEnumerable<CompanyDto>>
    {
        public string? Keyword { get; set; }
        public DateTime? EmployeeDateOfBirthFrom { get; set; }
        public DateTime? EmployeeDateOfBirthTo { get; set; }
        public ISet<JobTitle>? EmployeeJobTitles { get; set; }
    }
}
