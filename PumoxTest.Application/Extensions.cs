﻿using Microsoft.Extensions.DependencyInjection;
using PumoxTest.Application.Commands;

namespace PumoxTest.Application
{
    public static class Extensions
    {
        public static void RegisterApplication(this IServiceCollection services)
        {
            services.AddCommandHandlers();
        }
    }
}
