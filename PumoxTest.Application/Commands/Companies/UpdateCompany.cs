﻿using PumoxTest.Core.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PumoxTest.Application.Commands.Companies
{
    public class UpdateCompany : ICommand
    {
        public long Id { get; }
        [Required]
        public string Name { get; }
        [Required]
        public int EstablishmentYear { get; }
        [Required]
        public IEnumerable<Employee> Employees { get; }

        public UpdateCompany(long id, string name, int establishmentYear, IEnumerable<Employee> employees)
        {
            Id = id;
            Name = name;
            EstablishmentYear = establishmentYear;
            Employees = employees;
        }

    }
}
