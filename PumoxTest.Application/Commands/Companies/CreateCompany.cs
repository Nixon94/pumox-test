﻿using PumoxTest.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PumoxTest.Application.Commands.Companies
{
    public class CreateCompany : ICommand
    {
        public long Id { get; }
        [Required]
        public string Name { get; }
        [Required]
        public int EstablishmentYear { get; }
        [Required]
        public IEnumerable<Employee> Employees { get; }

        public CreateCompany(long? id, string name, int establishmentYear, IEnumerable<Employee> employees)
        {
            Id = id ?? BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0);
            Name = name;
            EstablishmentYear = establishmentYear;
            Employees = employees;
        }
    }
}
