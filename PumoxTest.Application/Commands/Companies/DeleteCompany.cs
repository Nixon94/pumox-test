﻿namespace PumoxTest.Application.Commands.Companies
{
    public class DeleteCompany : ICommand
    {
        public long Id { get; }
        public DeleteCompany(long id)
        {
            Id = id;
        }
    }
}
