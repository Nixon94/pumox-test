﻿using PumoxTest.Application.Exceptions.Company;
using PumoxTest.Core.Repositories;
using System.Threading.Tasks;

namespace PumoxTest.Application.Commands.Companies.Handlers
{

    internal class DeleteCompanyHandler : ICommandHandler<DeleteCompany>
    {
        private readonly ICompanyRepository _repository;

        public DeleteCompanyHandler(ICompanyRepository repository)
        {
            _repository = repository;
        }

        public async Task HandleAsync(DeleteCompany command)
        {
            var company = await _repository.GetAsync(command.Id);

            if (company is null) throw new CompanyDoesntExistException(command.Id);

            await _repository.DeleteAsync(company);
        }
    }
}
