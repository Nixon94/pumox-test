﻿using PumoxTest.Application.Exceptions.Company;
using PumoxTest.Core.Repositories;
using System.Threading.Tasks;

namespace PumoxTest.Application.Commands.Companies.Handlers
{

    class UpdateCompanyHandler : ICommandHandler<UpdateCompany>
    {
        private readonly ICompanyRepository _repository;

        public UpdateCompanyHandler(ICompanyRepository repository)
        {
            _repository = repository;

        }

        public async Task HandleAsync(UpdateCompany command)
        {
            var company = await _repository.GetAsync(command.Id);

            if (company is null)
            {
                throw new CompanyDoesntExistException(command.Id);
            }

            company.SetName(command.Name);
            company.SetEstablishmentYear(command.EstablishmentYear);
            company.AddEmployees(command.Employees);

            await _repository.UpdateAsync(company);
        }
    }
}
