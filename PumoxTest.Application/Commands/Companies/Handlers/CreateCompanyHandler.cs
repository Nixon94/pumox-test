﻿using PumoxTest.Application.Exceptions.Company;
using PumoxTest.Core.Entities;
using PumoxTest.Core.Repositories;
using System.Threading.Tasks;

namespace PumoxTest.Application.Commands.Companies.Handlers
{

    internal class CreateCompanyHandler : ICommandHandler<CreateCompany>
    {

        private readonly ICompanyRepository _repository;

        public CreateCompanyHandler(ICompanyRepository repository)
        {
            _repository = repository;
        }

        public async Task HandleAsync(CreateCompany command)
        {
            var company = await _repository.GetAsync(command.Id);

            if (company != null) throw new CompanyAlreadyExistException(command.Id);

            var newCompany = new Company(command.Id, command.Name, command.EstablishmentYear, command.Employees);

            await _repository.AddAsync(newCompany);
        }
    }
}
