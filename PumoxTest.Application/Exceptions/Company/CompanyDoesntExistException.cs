﻿using PumoxTest.Core.Exceptions;

namespace PumoxTest.Application.Exceptions.Company
{
    class CompanyDoesntExistException : DomainException
    {
        public CompanyDoesntExistException(long id)
            : base($"Company with id: {id} does not exist") { }
    }
}
