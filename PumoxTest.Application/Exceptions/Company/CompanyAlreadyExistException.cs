﻿using PumoxTest.Core.Exceptions;

namespace PumoxTest.Application.Exceptions.Company
{
    class CompanyAlreadyExistException : DomainException
    {
        public CompanyAlreadyExistException(long id)
            : base($"Company with id: {id} already exists") { }
    }
}
