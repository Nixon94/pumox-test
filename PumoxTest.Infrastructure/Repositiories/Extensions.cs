﻿using Microsoft.Extensions.DependencyInjection;
using PumoxTest.Core.Repositories;

namespace PumoxTest.Infrastructure.Repositiories
{
    internal static class Extensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<ICompanyRepository, CompanyDatabaseRepository>();
        }
    }
}
