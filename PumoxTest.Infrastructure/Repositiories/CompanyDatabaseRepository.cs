﻿using PumoxTest.Core.Entities;
using PumoxTest.Core.Repositories;
using PumoxTest.Infrastructure.Databases;
using PumoxTest.Infrastructure.Databases.NHibernate.Entities;
using PumoxTest.Infrastructure.Databases.NHibernate.Entities.Extensions;
using System.Threading.Tasks;

namespace PumoxTest.Infrastructure.Repositiories
{
    internal class CompanyDatabaseRepository : ICompanyRepository
    {
        private readonly IRepository<CompanyEntity> _repository;

        public CompanyDatabaseRepository(IRepository<CompanyEntity> repository)
            => _repository = repository;

        public async Task<Company> GetAsync(long id)
        {
            var entity = await _repository.GetAsync(id);
            return entity?.AsCompany();
        }

        public Task AddAsync(Company company)
            => _repository.AddAsync(company.AsEntity());

        public Task UpdateAsync(Company company)
            => _repository.UpdateAsync(company.AsEntity());

        public Task DeleteAsync(Company company)
            => _repository.DeleteAsync(company.AsEntity());
    }
}
