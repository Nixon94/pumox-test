﻿using Microsoft.Extensions.Logging;
using PumoxTest.Application.Commands;
using PumoxTest.Application.Commands.Companies;
using System.Threading.Tasks;

namespace PumoxTest.Infrastructure.IoC.Companies
{

    internal class DeleteCompanyDecorator : ICommandHandler<DeleteCompany>
    {
        private readonly ICommandHandler<DeleteCompany> _handler;
        private readonly ILogger<DeleteCompanyDecorator> _logger;

        public DeleteCompanyDecorator(ICommandHandler<DeleteCompany> handler,
            ILogger<DeleteCompanyDecorator> logger)
        {
            _handler = handler;
            _logger = logger;
        }

        public async Task HandleAsync(DeleteCompany command)
        {
            _logger.LogInformation($"\n*** Started processing {nameof(DeleteCompany)} ***");
            await _handler.HandleAsync(command);
            _logger.LogInformation($"*** Finished processing {nameof(DeleteCompany)} ***\n");
        }
    }
}
