﻿using Microsoft.Extensions.Logging;
using PumoxTest.Application.Commands;
using PumoxTest.Application.Commands.Companies;
using System.Threading.Tasks;

namespace PumoxTest.Infrastructure.IoC.Companies
{

    internal class CreateCompanyDecorator : ICommandHandler<CreateCompany>
    {
        private readonly ICommandHandler<CreateCompany> _handler;
        private readonly ILogger<CreateCompanyDecorator> _logger;

        public CreateCompanyDecorator(ICommandHandler<CreateCompany> handler,
            ILogger<CreateCompanyDecorator> logger)
        {
            _handler = handler;
            _logger = logger;
        }

        public async Task HandleAsync(CreateCompany command)
        {
            _logger.LogInformation($"\n*** Started processing {nameof(CreateCompany)} ***");
            await _handler.HandleAsync(command);
            _logger.LogInformation($"*** Finished processing {nameof(CreateCompany)} ***\n");
        }
    }
}
