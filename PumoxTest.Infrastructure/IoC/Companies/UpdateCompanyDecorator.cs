﻿using Microsoft.Extensions.Logging;
using PumoxTest.Application.Commands;
using PumoxTest.Application.Commands.Companies;
using System.Threading.Tasks;

namespace PumoxTest.Infrastructure.IoC.Companies
{

    internal class UpdateCompanyDecorator : ICommandHandler<UpdateCompany>
    {
        private readonly ICommandHandler<UpdateCompany> _handler;
        private readonly ILogger<UpdateCompanyDecorator> _logger;

        public UpdateCompanyDecorator(ICommandHandler<UpdateCompany> handler,
            ILogger<UpdateCompanyDecorator> logger)
        {
            _handler = handler;
            _logger = logger;
        }

        public async Task HandleAsync(UpdateCompany command)
        {
            _logger.LogInformation($"\n*** Started processing {nameof(UpdateCompany)} ***");
            await _handler.HandleAsync(command);
            _logger.LogInformation($"*** Finished processing {nameof(UpdateCompany)} ***\n");
        }
    }
}
