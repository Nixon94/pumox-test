﻿using Microsoft.Extensions.DependencyInjection;
using PumoxTest.Application.Commands;
using PumoxTest.Application.Commands.Companies;
using PumoxTest.Infrastructure.IoC.Companies;

namespace PumoxTest.Infrastructure.IoC
{
    internal static class Extensions
    {
        public static void AddDecorators(this IServiceCollection services)
        {
            services.Decorate<ICommandHandler<CreateCompany>, CreateCompanyDecorator>();
            services.Decorate<ICommandHandler<UpdateCompany>, UpdateCompanyDecorator>();
            services.Decorate<ICommandHandler<DeleteCompany>, DeleteCompanyDecorator>();
        }
    }
}
