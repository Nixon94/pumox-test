﻿using PumoxTest.Application.DTO;
using PumoxTest.Core.Entities;
using System.Collections.Generic;
using System.Linq;

namespace PumoxTest.Infrastructure.Databases.NHibernate.Entities.Extensions
{
    internal static class EmployeeExtensions
    {
        public static EmployeeEntity AsEntity(this Employee employee)
        {
            return new EmployeeEntity
            {
                FirstName = employee.FirstName,
                CompanyId=employee.CompanyId,
                LastName = employee.LastName,
                JobTitle = employee.JobTitle,
                DateOfBirth = employee.DateOfBirth
                
            };
        }
        public static IEnumerable<EmployeeEntity> AsEntities(this IEnumerable<Employee> employees)
        {
            return employees.Select(e => e.AsEntity());
        }

        public static Employee AsEmployee(this EmployeeEntity entity)
        {
            return new Employee(entity.FirstName,entity.LastName,entity.CompanyId,entity.JobTitle, entity.DateOfBirth);
        }
        public static IEnumerable<Employee> AsEmployees(this IEnumerable<EmployeeEntity> entities)
        {
            return entities.Select(e => e.AsEmployee());
        }

        public static EmployeeDto AsDto(this EmployeeEntity entity)
        {
            return new EmployeeDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                JobTitle = entity.JobTitle,
                DateOfBirth = entity.DateOfBirth
            };
        }

        public static IEnumerable<EmployeeDto> AsDtos(this IEnumerable<EmployeeEntity> employees)
        {
            return employees.Select(a => a.AsDto());
        }
    }
}
