﻿using PumoxTest.Application.DTO;
using PumoxTest.Core.Entities;
using System.Collections.Generic;
using System.Linq;

namespace PumoxTest.Infrastructure.Databases.NHibernate.Entities.Extensions
{
    internal static class CompanyExtensions
    {
        public static CompanyEntity AsEntity(this Company company)
        {
            return new CompanyEntity
            {
                Id = company.Id,
                Name = company.Name,
                EstablishmentYear = company.EstablishmentYear,
                Employees = company.Employees.AsEntities().ToList()
            };
        }

        public static Company AsCompany(this CompanyEntity entity)
        {
            return new Company(entity.Id, entity.Name, entity.EstablishmentYear, entity.Employees.AsEmployees());
        }

        public static CompanyDto AsDto(this CompanyEntity entity)
        {
            return new CompanyDto
            {
                Id = entity.Id,
                Name = entity.Name,
                EstablishmentYear = entity.EstablishmentYear,
                Employees = entity.Employees.AsDtos().ToList()
            };
        }

        public static IEnumerable<CompanyDto> AsDtos(this IEnumerable<CompanyEntity> companies)
        {
            return companies.Select(a => a.AsDto());
        }
    }
}
