﻿using NHibernate.Mapping.ByCode;

namespace PumoxTest.Infrastructure.Databases.NHibernate.Entities.Mappings
{
    internal static class Extensions
    {
        public static void AddMappings(this ModelMapper mapper)
        {
            mapper.AddMapping<EmployeeEntityMapping>();
            mapper.AddMapping<CompanyEntityMapping>();
        }
    }
}
