﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using PumoxTest.Core.Enums;

namespace PumoxTest.Infrastructure.Databases.NHibernate.Entities.Mappings
{
    internal class EmployeeEntityMapping : ClassMapping<EmployeeEntity>
    {
        public EmployeeEntityMapping()
        {
            Table("Employee");
            Id(x => x.Id, map => map.Generator(Generators.Identity));
            Property(c => c.FirstName);
            Property(c => c.LastName);
            Property(c => c.DateOfBirth);
            Property(c => c.JobTitle, e => e.Type<EnumStringType<JobTitle>>());
        }
    }
}
