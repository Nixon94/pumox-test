﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;

namespace PumoxTest.Infrastructure.Databases.NHibernate.Entities.Mappings
{
    internal class CompanyEntityMapping : ClassMapping<CompanyEntity>
    {
        public CompanyEntityMapping()
        {
            Table("Company");
            Id(e => e.Id);
            Property(x => x.Name);
            Property(x => x.EstablishmentYear);
            Bag(x => x.Employees,
            m =>
            {
                m.Table("Employee");
                m.Cascade(Cascade.All);
                m.Key(c => c.Column("CompanyId"));
            },
            map =>
            {
                map.OneToMany(p => p.Class(typeof(EmployeeEntity)));
            });
            Property(e => e.IsDeleted, map =>
            {
                map.NotNullable(true);
                map.Type<YesNoType>();
            });

        }
    }
}
