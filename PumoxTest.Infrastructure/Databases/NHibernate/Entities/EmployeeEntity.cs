﻿using PumoxTest.Core.Enums;
using System;

namespace PumoxTest.Infrastructure.Databases.NHibernate.Entities
{
    internal class EmployeeEntity
    {
        public virtual int? Id { get; set; }
        public virtual long CompanyId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
        public virtual JobTitle JobTitle { get; set; }
    }
}
