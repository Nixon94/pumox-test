﻿using System.Collections.Generic;

namespace PumoxTest.Infrastructure.Databases.NHibernate.Entities
{
    internal class CompanyEntity : SoftDeletableDbModel<long>
    {
        public virtual string Name { get; set; }
        public virtual int EstablishmentYear { get; set; }
        public virtual IEnumerable<EmployeeEntity> Employees { get; set; }
    }
}
