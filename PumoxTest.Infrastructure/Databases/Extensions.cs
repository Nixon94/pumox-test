﻿using Microsoft.Extensions.DependencyInjection;
using PumoxTest.Infrastructure.Databases.NHibernate;
using PumoxTest.Infrastructure.Options;

namespace PumoxTest.Infrastructure.Databases
{
    internal static class Extensions
    {
        public static void AddDatabase(this IServiceCollection services)
        {
            services.AddOption<DatabaseOptions>("Database");
            services.AddNHibernate();
        }
    }
}
