﻿using Microsoft.Extensions.DependencyInjection;
using PumoxTest.Infrastructure.Databases;
using PumoxTest.Infrastructure.Dispatchers;
using PumoxTest.Infrastructure.IoC;
using PumoxTest.Infrastructure.Queries;
using PumoxTest.Infrastructure.Repositiories;

namespace PumoxTest.Infrastructure
{
    public static class Extensions
    {
        public static void RegisterInfrastructure(this IServiceCollection services)
        {
            services.AddDispatchers();
            services.AddQueryHandlers();
            services.AddDecorators();
            services.AddRepositories();
            services.AddDatabase();
        }
    }
}
