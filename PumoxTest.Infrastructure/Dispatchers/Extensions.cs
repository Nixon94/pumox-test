﻿using Microsoft.Extensions.DependencyInjection;
using PumoxTest.Application.Commands.Dispatchers;
using PumoxTest.Application.Queries.Dispatchers;

namespace PumoxTest.Infrastructure.Dispatchers
{
    internal static class Extensions
    {
        public static void AddDispatchers(this IServiceCollection services)
        {
            services.AddTransient<IQueryDispatcher, QueryDispatcher>();
            services.AddTransient<ICommandDispatcher, CommandDispatcher>();
        }
    }
}
