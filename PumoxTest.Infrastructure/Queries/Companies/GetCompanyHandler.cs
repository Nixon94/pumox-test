﻿using PumoxTest.Application.DTO;
using PumoxTest.Application.Queries;
using PumoxTest.Application.Queries.Companies;
using PumoxTest.Infrastructure.Databases;
using PumoxTest.Infrastructure.Databases.NHibernate.Entities;
using PumoxTest.Infrastructure.Databases.NHibernate.Entities.Extensions;
using System.Threading.Tasks;

namespace PumoxTest.Infrastructure.Queries.Companies
{

    internal class GetCompanyHandler : IQueryHandler<GetCompany, CompanyDto>
    {
        private readonly IRepository<CompanyEntity> _repository;

        public GetCompanyHandler(IRepository<CompanyEntity> repository)
            => _repository = repository;

        public async Task<CompanyDto> HandleAsync(GetCompany query)
        {
            var company = await _repository.GetAsync(query.Id);
            return company?.AsDto();
        }
    }
}
