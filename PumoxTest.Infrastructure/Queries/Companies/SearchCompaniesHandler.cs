﻿using PumoxTest.Application.DTO;
using PumoxTest.Application.Queries;
using PumoxTest.Application.Queries.Companies;
using PumoxTest.Infrastructure.Databases;
using PumoxTest.Infrastructure.Databases.NHibernate.Entities;
using PumoxTest.Infrastructure.Databases.NHibernate.Entities.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PumoxTest.Infrastructure.Queries.Companies
{
    internal class SearchCompaniesHandler : IQueryHandler<SearchCompanies, IEnumerable<CompanyDto>>
    {
        private readonly IRepository<CompanyEntity> _repository;

        public SearchCompaniesHandler(IRepository<CompanyEntity> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<CompanyDto>> HandleAsync(SearchCompanies query)
        {
            IEnumerable<CompanyEntity> entities;
            if (query.Keyword is null)
                entities = await _repository.SearchAsync(e => e.Name.Contains(""));
            else
                entities = await _repository.SearchAsync(c => c.Name.Contains(query.Keyword)
                || c.Employees.Any(e => e.FirstName.Contains(query.Keyword))
                || c.Employees.Any(e => e.LastName.Contains(query.Keyword)));

            if (query.EmployeeDateOfBirthFrom.HasValue)
                entities = entities.Where(c => c.Employees.Any(e => e.DateOfBirth > query.EmployeeDateOfBirthFrom));

            if (query.EmployeeDateOfBirthTo.HasValue)
                entities = entities.Where(c => c.Employees.Any(e => e.DateOfBirth < query.EmployeeDateOfBirthTo));

            if (query.EmployeeJobTitles != null)
                if (query.EmployeeJobTitles.Any())
                    entities = entities.Where(c => c.Employees.Any(e => query.EmployeeJobTitles.Contains(e.JobTitle)));

            return entities.AsDtos();
        }
    }
}
