﻿using Microsoft.Extensions.DependencyInjection;
using PumoxTest.Application.DTO;
using PumoxTest.Application.Queries;
using PumoxTest.Application.Queries.Companies;
using PumoxTest.Infrastructure.Queries.Companies;
using System.Collections.Generic;

namespace PumoxTest.Infrastructure.Queries
{
    internal static class Extensions
    {
        public static void AddQueryHandlers(this IServiceCollection services)
        {
            services.AddTransient<IQueryHandler<GetCompany, CompanyDto>, GetCompanyHandler>();
            services.AddTransient<IQueryHandler<SearchCompanies, IEnumerable<CompanyDto>>, SearchCompaniesHandler>();
        }
    }
}
